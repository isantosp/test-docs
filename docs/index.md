# MkDocs Example

Find more information on how this page was create in the [MkDocs GitLab repository](https://gitlab.cern.ch/authoring/documentation/mkdocs).

## Source files

The source files for this example can be found in the [MkDocs Example GitLab repository](https://gitlab.cern.ch/authoring/documentation/mkdocs-example).

## Theme

This example is using the [Material theme for MkDocs](https://squidfunk.github.io/mkdocs-material/). To use this theme yourself all you have to do is add the following in your MkDocs configuration file ([`mkdocs.yml`](https://gitlab.cern.ch/authoring/documentation/mkdocs-example/blob/master/mkdocs.yml)):
```yaml
theme:
    name: material
```

On your left you'll find the content structure of the entire site and on your right the table of conents of this specific page.
